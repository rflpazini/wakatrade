FROM adoptopenjdk/openjdk11-openj9:alpine-jre

WORKDIR /opt/app

ADD ./* /output/

EXPOSE 8081

COPY build/libs/*-all.jar wakatrade.jar
COPY build/resources/main/  .

CMD ["java", "-server", "-jar","-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-XX:+UseContainerSupport", "-XX:MaxRAMPercentage=85", "/opt/app/wakatrade.jar"]