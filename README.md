## WakaTrade

![java 11](https://img.shields.io/badge/java-11-orange.svg)
![micronaut 2.5.0](https://img.shields.io/badge/micronaut-2.5.0-272822.svg)

WakaTrade is a OrderBook created to Bid and Offer your Vibranium

---

## Running it

Just run your application with `gradle` command or using your IDE.

```ssh
$ ./gradlew clean run
```

and the application will be available the following URL

```ssh
http://localhost:8081/swagger/views/swagger-ui
```

### License

[MIT License](http://rflpazini.mit-license.org) :copyright: Rafael Pazini

