package com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.mapper;

import com.rflpazini.wakatrade.application.entrypoint.order.entity.OrderRequest;
import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.OrderEntity;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

public interface OrderMapper {

    OrderEntity fromDomain(Order order);

    Order toDomain(OrderEntity orderEntity);

    Order fromRequestToDomain(OrderRequest orderRequest);

    OrderEntity.Type fromDomain(Order.Type type);
}
