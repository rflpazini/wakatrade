package com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.mapper;

import com.rflpazini.wakatrade.application.entrypoint.wallet.entity.WalletRequest;
import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.WalletEntity;
import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;

public interface WalletMapper {

    WalletEntity fromDomain(Wallet wallet);

    Wallet toDomain(WalletEntity wallet);

    Wallet fromRequestToDomain(WalletRequest wallet);
}
