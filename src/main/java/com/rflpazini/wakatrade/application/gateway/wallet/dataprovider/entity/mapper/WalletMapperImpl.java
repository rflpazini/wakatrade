package com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.mapper;

import com.rflpazini.wakatrade.application.entrypoint.wallet.entity.WalletRequest;
import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.WalletEntity;
import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;

import javax.inject.Singleton;

@Singleton
public class WalletMapperImpl implements WalletMapper {

    @Override
    public WalletEntity fromDomain(final Wallet wallet) {
        return WalletEntity.builder()
                .id(wallet.getId())
                .quantity(wallet.getQuantity())
                .amount(wallet.getAmount())
                .createdAt(wallet.getCreatedAt())
                .updatedAt(wallet.getUpdatedAt())
                .build();
    }

    @Override
    public Wallet toDomain(final WalletEntity wallet) {
        return Wallet.builder()
                .id(wallet.getId())
                .amount(wallet.getAmount())
                .quantity(wallet.getQuantity())
                .createdAt(wallet.getCreatedAt())
                .updatedAt(wallet.getUpdatedAt())
                .build();
    }

    @Override
    public Wallet fromRequestToDomain(final WalletRequest wallet) {
        return Wallet.builder()
                .quantity(wallet.getQuantity())
                .amount(wallet.getAmount())
                .build();
    }
}
