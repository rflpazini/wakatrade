package com.rflpazini.wakatrade.application.gateway.order;

import com.rflpazini.wakatrade.application.gateway.order.dataprovider.OrderRepository;
import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.mapper.OrderMapper;
import com.rflpazini.wakatrade.domain.exceptions.OrderNotFound;
import com.rflpazini.wakatrade.domain.gateway.order.OrderDbGateway;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;
import io.micronaut.transaction.annotation.ReadOnly;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class OrderDbGatewayImpl implements OrderDbGateway {

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private OrderMapper orderMapper;

    @Override
    @Transactional
    public Order create(Order order) {
        var newOrder = orderRepository.save(orderMapper.fromDomain(order));
        logAction("create", newOrder);
        return orderMapper.toDomain(newOrder);
    }

    @Override
    @ReadOnly
    public Order findById(final Long id) {
        return orderRepository.findById(id)
                .map(orderMapper::toDomain)
                .orElseThrow(() -> new OrderNotFound(id));
    }

    @Override
    @ReadOnly
    public List<Order> findByType(final Order.Type type) {
        return orderRepository.findByType(orderMapper.fromDomain(type)).stream()
                .map(orderMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    @ReadOnly
    public List<Order> findByTypeAndValue(final Order.Type type, final double value) {
        return orderRepository.findByTypeAndValue(orderMapper.fromDomain(type), value).stream()
                .map(orderMapper::toDomain)
                .collect(Collectors.toList());
    }

    private void logAction(final String action, final Object order) {
        log.info("[{}]: {}", action.toUpperCase(Locale.ROOT), order.toString());
    }
}
