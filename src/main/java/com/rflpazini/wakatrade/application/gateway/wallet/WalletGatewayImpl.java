package com.rflpazini.wakatrade.application.gateway.wallet;

import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.WalletRepository;
import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.mapper.WalletMapper;
import com.rflpazini.wakatrade.domain.exceptions.WalletNotFound;
import com.rflpazini.wakatrade.domain.gateway.wallet.WalletGateway;
import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;
import io.micronaut.transaction.annotation.ReadOnly;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Locale;

@Slf4j
@Singleton
public class WalletGatewayImpl implements WalletGateway {

    @Inject
    private WalletRepository walletRepository;

    @Inject
    private WalletMapper walletMapper;

    @Override
    @Transactional
    public Wallet create(final Wallet wallet) {
        var newWallet = walletRepository.save(walletMapper.fromDomain(wallet));
        logAction("create", newWallet.getId());

        return walletMapper.toDomain(newWallet);
    }

    @Override
    @ReadOnly
    public Wallet findById(final Long id) {
        return walletRepository.findById(id)
                .map(walletMapper::toDomain)
                .orElseThrow(() -> new WalletNotFound(id));
    }

    @Override
    @Transactional
    public Wallet update(final Wallet wallet) {
        walletRepository.findById(wallet.getId())
                .ifPresent(it -> walletRepository.update(
                        it.builder()
                                .id(wallet.getId())
                                .amount(wallet.getAmount())
                                .quantity(wallet.getQuantity())
                                .build()));
        logAction("update", wallet.getId());

        return wallet;
    }

    private void logAction(final String action, final Long id) {
        log.info("[{}]: Wallet with ID {}", action.toUpperCase(Locale.ROOT), id);
    }
}
