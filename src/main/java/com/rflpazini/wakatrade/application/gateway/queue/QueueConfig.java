package com.rflpazini.wakatrade.application.gateway.queue;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public enum QueueConfig {
    INSTANCE;

    private final Queue<Double> bid;
    private final Queue<Double> offer;

    QueueConfig() {
        this.bid = new PriorityQueue<>(100, Collections.reverseOrder());
        this.offer = new PriorityQueue<>();
    }

    public Queue<Double> bid() {
        return bid;
    }

    public Queue<Double> offer() {
        return offer;
    }
}
