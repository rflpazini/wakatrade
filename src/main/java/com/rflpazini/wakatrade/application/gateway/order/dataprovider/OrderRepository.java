package com.rflpazini.wakatrade.application.gateway.order.dataprovider;

import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.OrderEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    OrderEntity findByName(final String name);

    List<OrderEntity> findByType(final OrderEntity.Type type);

    List<OrderEntity> findByTypeAndValue(final OrderEntity.Type type, final double value);
}
