package com.rflpazini.wakatrade.application.gateway.wallet.dataprovider;

import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.WalletEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {
}
