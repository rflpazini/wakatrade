package com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.mapper;

import com.rflpazini.wakatrade.application.entrypoint.order.entity.OrderRequest;
import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.OrderEntity;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import javax.inject.Singleton;
import java.util.Locale;

@Singleton
public class OrderMapperImpl implements OrderMapper {

    @Override
    public OrderEntity fromDomain(final Order order) {
        return OrderEntity.builder()
                .id(order.getId())
                .name(order.getName())
                .quantity(order.getQuantity())
                .value(order.getValue())
                .type(OrderEntity.Type.valueOf(order.getType().name()))
                .createdAt(order.getCreatedAt())
                .updatedAt(order.getUpdatedAt())
                .build();
    }

    @Override
    public Order toDomain(final OrderEntity orderEntity) {
        return Order.builder()
                .id(orderEntity.getId())
                .name(orderEntity.getName())
                .quantity(orderEntity.getQuantity())
                .value(orderEntity.getValue())
                .type(Order.Type.valueOf(orderEntity.getType().name()))
                .createdAt(orderEntity.getCreatedAt())
                .updatedAt(orderEntity.getUpdatedAt())
                .build();
    }

    @Override
    public Order fromRequestToDomain(OrderRequest orderRequest) {
        return Order.builder()
                .name(orderRequest.getName().toUpperCase(Locale.ROOT))
                .quantity(orderRequest.getQuantity())
                .value(orderRequest.getValue())
                .type(Order.Type.valueOf(orderRequest.getType().name()))
                .build();
    }

    @Override
    public OrderEntity.Type fromDomain(Order.Type type) {
        return OrderEntity.Type.valueOf(type.name());
    }
}
