package com.rflpazini.wakatrade.application.gateway.order;

import com.rflpazini.wakatrade.application.gateway.queue.QueueConfig;
import com.rflpazini.wakatrade.domain.gateway.order.OrderGateway;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import javax.inject.Singleton;
import java.util.Queue;

@Singleton
public class OrderGatewayImpl implements OrderGateway {

    private static final Queue<Double> bidQueue = QueueConfig.INSTANCE.bid();
    private static final Queue<Double> offerQueue = QueueConfig.INSTANCE.offer();

    @Override
    public void publish(Order order) {
        if (order.getType() == Order.Type.BID) bidQueue.add(order.getValue());
        else offerQueue.add(order.getValue());
    }
}
