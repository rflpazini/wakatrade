package com.rflpazini.wakatrade.application.entrypoint.wallet;

import com.rflpazini.wakatrade.application.entrypoint.wallet.entity.WalletRequest;
import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.WalletEntity;
import com.rflpazini.wakatrade.application.gateway.wallet.dataprovider.entity.mapper.WalletMapper;
import com.rflpazini.wakatrade.domain.usecase.wallet.CreateWallet;
import com.rflpazini.wakatrade.domain.usecase.wallet.RetrieveWallet;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.inject.Inject;

@Tag(name = "wallet")
@Controller("/wallet")
public class WalletEntrypoint {

    @Inject
    private RetrieveWallet retrieveWallet;

    @Inject
    private CreateWallet createWallet;

    @Inject
    private WalletMapper walletMapper;

    @Get(uri = "/{id}")
    public HttpResponse<WalletEntity> getWallet(@PathVariable Long id) {
        return HttpResponse.ok(walletMapper.fromDomain(retrieveWallet.byId(id)));
    }

    @Post
    public HttpResponse<WalletEntity> createWallet(@Body WalletRequest walletRequest) {
        var savedWallet = createWallet.execute(walletMapper.fromRequestToDomain(walletRequest));
        return HttpResponse.ok(
                walletMapper.fromDomain(savedWallet)
        );
    }
}
