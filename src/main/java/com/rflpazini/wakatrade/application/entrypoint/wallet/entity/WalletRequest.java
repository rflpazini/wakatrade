package com.rflpazini.wakatrade.application.entrypoint.wallet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletRequest {

    private double amount;

    private int quantity;
}
