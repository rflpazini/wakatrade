package com.rflpazini.wakatrade.application.entrypoint.order.entity;

import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {
    private String name;

    private Type type;

    private int quantity;

    private double value;

    public enum Type {
        BID, OFFER
    }
}
