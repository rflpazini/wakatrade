package com.rflpazini.wakatrade.application.entrypoint.order;

import com.rflpazini.wakatrade.application.entrypoint.order.entity.OrderRequest;
import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.OrderEntity;
import com.rflpazini.wakatrade.application.gateway.order.dataprovider.entity.mapper.OrderMapper;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;
import com.rflpazini.wakatrade.domain.usecase.order.CreateOrder;
import com.rflpazini.wakatrade.domain.usecase.order.RetrieveOrder;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Tag(name = "order")
@Controller("/order")
public class OrderEntrypoint {

    @Inject
    private CreateOrder createOrder;

    @Inject
    private RetrieveOrder retrieveOrder;

    @Inject
    private OrderMapper orderMapper;

    @Get
    public HttpResponse<List<OrderEntity>> retrieveByType(@QueryValue("type") OrderRequest.Type type) {
        return HttpResponse.ok(
                retrieveOrder.byType(Order.Type.valueOf(type.name().toUpperCase(Locale.ROOT))).stream()
                        .map(orderMapper::fromDomain)
                        .collect(Collectors.toList()));
    }

    @Get("/{id}")
    public HttpResponse<OrderEntity> retrieveById(@PathVariable Long id) {
        return HttpResponse.ok(orderMapper.fromDomain(retrieveOrder.byId(id)));
    }

    @Post
    public HttpResponse<OrderEntity> orderCreation(@Body OrderRequest orderRequest) throws URISyntaxException {
        var newOrder = createOrder.apply(orderMapper.fromRequestToDomain(orderRequest));
        var location = new URI("http://localhost:8081/order/" + newOrder.getId());

        return HttpResponse.created(location);
    }
}
