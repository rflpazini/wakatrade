package com.rflpazini.wakatrade.application.exceptions;

import com.rflpazini.wakatrade.domain.exceptions.NotFoundException;
import io.micronaut.http.HttpStatus;

import javax.inject.Singleton;

@Singleton
public class GlobalHandlerImpl extends GlobalHandler<Throwable> {

    @Override
    public HttpStatus statusCode(Throwable exception) {
        if (exception instanceof NotFoundException) return HttpStatus.NOT_FOUND;
        else return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
