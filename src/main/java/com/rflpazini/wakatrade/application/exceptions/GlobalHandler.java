package com.rflpazini.wakatrade.application.exceptions;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
abstract class GlobalHandler<T extends Throwable> implements ExceptionHandler<T, HttpResponse<JsonError>> {

    @Override
    public HttpResponse<JsonError> handle(HttpRequest request, T exception) {
        var body = new JsonError(exception.getMessage()).path(request.getUri().getPath());
        log.error(exception.getMessage());

        return HttpResponse.status(statusCode(exception)).body(body);
    }

    public abstract HttpStatus statusCode(T exception);
}