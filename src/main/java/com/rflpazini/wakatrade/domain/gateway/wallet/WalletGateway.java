package com.rflpazini.wakatrade.domain.gateway.wallet;

import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;

public interface WalletGateway {

    Wallet create(Wallet wallet);

    Wallet findById(Long id);

    Wallet update(Wallet wallet);

}
