package com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Wallet {

    private Long id;

    private double amount;

    private int quantity;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
