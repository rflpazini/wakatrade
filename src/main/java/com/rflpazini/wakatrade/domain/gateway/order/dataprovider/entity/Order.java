package com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private Long id;

    private String name;

    private Type type;

    private int quantity;

    private double value;

    private Instant createdAt;

    private Instant updatedAt;

    public enum Type {
        BID, OFFER
    }
}
