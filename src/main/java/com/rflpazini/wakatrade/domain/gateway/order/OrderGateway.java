package com.rflpazini.wakatrade.domain.gateway.order;

import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

public interface OrderGateway {
    void publish(Order order);
}
