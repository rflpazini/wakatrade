package com.rflpazini.wakatrade.domain.gateway.order;

import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import java.util.List;

public interface OrderDbGateway {

    Order create(Order order);

    Order findById(Long id);

    List<Order> findByType(Order.Type type);

    List<Order> findByTypeAndValue(Order.Type type, double value);
}
