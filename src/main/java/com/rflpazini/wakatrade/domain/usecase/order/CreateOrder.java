package com.rflpazini.wakatrade.domain.usecase.order;

import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

public interface CreateOrder {

    Order apply(Order order);
}
