package com.rflpazini.wakatrade.domain.usecase.wallet;

import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;

public interface CreateWallet {
    Wallet execute(Wallet wallet);
}
