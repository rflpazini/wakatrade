package com.rflpazini.wakatrade.domain.usecase.order;

import com.rflpazini.wakatrade.domain.gateway.order.OrderDbGateway;
import com.rflpazini.wakatrade.domain.gateway.order.OrderGateway;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import javax.inject.Inject;

public class CreateOrderUseCase implements CreateOrder {

    @Inject
    private OrderGateway orderGateway;

    @Inject
    private OrderDbGateway orderDbGateway;

    @Override
    public Order apply(Order order) {
        orderGateway.publish(order);

        return orderDbGateway.create(order);
    }
}
