package com.rflpazini.wakatrade.domain.usecase.wallet;

import com.rflpazini.wakatrade.domain.gateway.wallet.WalletGateway;
import com.rflpazini.wakatrade.domain.gateway.wallet.dataprovider.entity.Wallet;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RetrieveWalletUseCase implements RetrieveWallet {

    @Inject
    private WalletGateway walletGateway;

    @Override
    public Wallet byId(Long id) {
        return walletGateway.findById(id);
    }
}
