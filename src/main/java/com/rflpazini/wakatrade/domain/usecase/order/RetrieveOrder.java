package com.rflpazini.wakatrade.domain.usecase.order;

import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import java.util.List;

public interface RetrieveOrder {

    Order byId(Long id);

    List<Order> byType(Order.Type type);

    List<Order> byTypeAndValue(Order.Type type, double value);
}
