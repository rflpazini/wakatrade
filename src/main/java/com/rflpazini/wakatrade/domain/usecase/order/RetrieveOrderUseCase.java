package com.rflpazini.wakatrade.domain.usecase.order;

import com.rflpazini.wakatrade.domain.gateway.order.OrderDbGateway;
import com.rflpazini.wakatrade.domain.gateway.order.dataprovider.entity.Order;

import javax.inject.Inject;
import java.util.List;

public class RetrieveOrderUseCase implements RetrieveOrder {

    @Inject
    private OrderDbGateway orderDbGateway;

    @Override
    public Order byId(final Long id) {
        return orderDbGateway.findById(id);
    }

    @Override
    public List<Order> byType(final Order.Type type) {
        return orderDbGateway.findByType(type);
    }

    @Override
    public List<Order> byTypeAndValue(Order.Type type, double value) {
        return null;
    }
}
