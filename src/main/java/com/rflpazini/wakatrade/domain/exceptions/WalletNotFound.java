package com.rflpazini.wakatrade.domain.exceptions;

public class WalletNotFound extends NotFoundException {
    public WalletNotFound(final Long id) {
        super(String.format("Wallet with ID [%d] not found", id));
    }
}
