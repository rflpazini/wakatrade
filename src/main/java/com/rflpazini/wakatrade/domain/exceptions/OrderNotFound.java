package com.rflpazini.wakatrade.domain.exceptions;

public class OrderNotFound extends NotFoundException{
    public OrderNotFound(final Long id) {
        super(String.format("Order with ID [%d] not found", id));
    }
}
