package com.rflpazini.wakatrade;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(title = "WakaTrade",
                version = "1.0",
                description = "WakaTrade is a OrderBook created to Bid and Offer your Vibranium",
                license = @License(name = "MIT", url = "https://rflpazini.mit-license.org")
        )
)
public class WakaTradeApplication {

    public static void main(String[] args) {
        Micronaut.run(WakaTradeApplication.class, args);
    }
}
