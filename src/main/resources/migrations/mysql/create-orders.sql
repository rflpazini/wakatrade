CREATE TABLE IF NOT EXISTS orders
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(10)            NOT NULL,
    type       INT                   NOT NULL,
    quantity   INT                   NOT NULL,
    value      DOUBLE                NOT NULL,
    created_at DATETIME              NOT NULL,
    updated_at DATETIME              NOT NULL,
    PRIMARY KEY (id)
);