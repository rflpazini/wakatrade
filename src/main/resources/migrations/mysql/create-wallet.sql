CREATE TABLE IF NOT EXISTS wallet
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    amount     DOUBLE                NOT NULL,
    quantity   INT                   NOT NULL,
    created_at DATETIME              NOT NULL,
    updated_at DATETIME              NOT NULL,
    PRIMARY KEY (id)
);